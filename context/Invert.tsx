"use client";

export function Invert() {}

import React, { createContext, useContext, useState } from "react";

interface State {
  filter: string;
}

interface InvertState extends State {
  setStyleLayout: React.Dispatch<React.SetStateAction<State>>;
}

const Context = createContext<InvertState>({
  filter: "invert(0)",
  setStyleLayout: () => {},
});

export function InvertProvider({ children }: { children: React.ReactNode }) {
  const [styleLayout, setStyleLayout] = useState<State>({
    filter: "invert(0)",
  });

  return (
    <Context.Provider value={{ ...styleLayout, setStyleLayout }}>
      {children}
    </Context.Provider>
  );
}

export function useInvertContext() {
  return useContext(Context);
}
