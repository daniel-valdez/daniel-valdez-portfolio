import Homebackground from "./components/HomeBackground/HomeBackground";
import Maincontent from "./components/MainContent/MainContent";
import Navbar from "./components/Navbar/Navbar";

export default function Home() {
  return (
    <>
      <main>
        <section>
          <Navbar />
          <Maincontent />
          <Homebackground />
        </section>
      </main>
      <svg xmlns="http://www.w3.org/2000/svg">
        <filter id="turbulence" x="0" y="0" width="100dvw" height="100dvh">
          <feTurbulence
            id="sea-filter"
            baseFrequency="0.02 0.05"
            numOctaves="3"
            seed="2"
          />
          <feDisplacementMap in="SourceGraphic" scale="7" />
          <animate
            attributeName="baseFrequency"
            dur="60s"
            keyTimes="0; 0.5; 1"
            repeatCount="indefinite"
            values="0.02 0.06; 0.04 0.08; 0.02 0.06"
            xlinkHref="#sea-filter"
          />
        </filter>
      </svg>
    </>
  );
}
