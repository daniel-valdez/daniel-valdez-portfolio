import { InvertProvider } from "@/context/Invert";
import type { Metadata } from "next";
import classes from "./layout.module.css";
import "./globals.css";

export const metadata: Metadata = {
  title: "Welcome",
  description: `Welcome to my website! I'm Daniel Valdez, a web developer, and also an economist, who specializes in designing and developing
          front-end experiences to organizations, companies, start-ups and
          individuals.
          I use my passion and skills to create beautiful and scalable web
          products tailor-made for them.
          Are you minding a project and thinking we can make something great
          together? Let's talk over email.`,
  keywords:
    "Daniel, Valdez, Montalvan, Daniel Valdez Montalvan, Portfolio, Web Developer, Full Stack Web Developer, Full Stack Developer, Website, Developer, Peru, Brasil, Brazil, US, United States, Freelance",
  icons: {
    icon: [
      { url: "/favicon.ico" },
      { url: "/favicon-32x32.png", type: "image/png", sizes: "32x32" },
      { url: "/favicon-16x16.png", type: "image/png", sizes: "16x16" },
    ],
    other: [
      {
        rel: "apple-touch-icon",
        sizes: "180x180",
        url: "/apple-touch-icon.png",
      },
    ],
  },
  manifest: "site.webmanifest",
};

export const viewport = {
  themeColor: "#FFFFFF",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <InvertProvider>
        <body className={classes.main_body}>{children}</body>
      </InvertProvider>
    </html>
  );
}
