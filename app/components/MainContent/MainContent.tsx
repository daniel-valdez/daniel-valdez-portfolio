"use client";
import React from "react";
import Link from "next/link";
import classes from "./MainContent.module.css";
import { soulmazeItalic, soulmazeBrush } from "../../fonts/fonts";
import { useInvertContext } from "../../../context/Invert";

function Maincontent() {
  const { filter } = useInvertContext();

  return (
    <div className={classes.container} style={{ filter: filter }}>
      <div className={classes.container_name}>
        <div className={classes.container_effect}>
          <h1 className={soulmazeItalic.className}>
            DANIEL
            <br />
            VALDEZ
          </h1>
        </div>
      </div>
      <div className={classes.container_actionquote}>
        <div className={classes.container_effect}>
          <h2 className={soulmazeItalic.className}>WEB DEVELOPER</h2>
          <blockquote className={soulmazeBrush.className}>
            if it doesn&apos;t challenge you, it doesn&apos;t change you...
          </blockquote>
          <div className={classes.container_button}>
            <button>
              <Link href="/work">
                <span className={soulmazeBrush.className}>
                  <b>&rarr; </b>
                  enjoy the waves
                </span>
              </Link>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Maincontent;
