"use client";
import React, { useState } from "react";
import classes from "./Menu.module.css";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";
import { soulmaze } from "../../fonts/fonts";

function Menu({
  isMenuListOpen,
  toggle,
}: {
  isMenuListOpen: boolean;
  toggle: () => void;
}) {
  const router = useRouter();
  const pathname = usePathname();

  const [bckgdImg, setBckgdImg] = useState(classes.background_image);
  const [styleImg, setStyleImg] = useState({
    opacity: 1,
    transform: "translateY(0vh)",
  });
  const [bckgdImgMask, setBckgdImgMask] = useState<string | undefined>("");

  const hover = (
    e: React.MouseEvent,
    classBckgdImage: string,
    classBckgdImageMask?: string
  ) => {
    e.preventDefault();
    setStyleImg((ed) => ({ ...ed, opacity: 0, transform: "translateY(0vh)" }));

    setTimeout(() => {
      setBckgdImgMask(classBckgdImageMask);
      setBckgdImg(classBckgdImage);
      setStyleImg((edi) => ({
        ...edi,
        opacity: 1,
        transform: "translateY(0vh)",
      }));
    }, 2000);
  };

  const hoveroff = () => {
    setBckgdImg(bckgdImg);
  };

  return (
    <div
      className={
        isMenuListOpen
          ? `${classes.menu_wrapper} ${classes.active}`
          : `${classes.menu_wrapper}`
      }
    >
      <div className={classes.background_menu_wrapper}>
        <div
          className={bckgdImg}
          style={
            isMenuListOpen
              ? styleImg
              : { opacity: 0, transform: "translateY(0vh)" }
          }
        >
          <div className={bckgdImgMask}></div>
        </div>
        <svg>
          <filter id="turbulence" x="0" y="0" width="100%" height="100%">
            <feTurbulence
              id="sea-filter"
              numOctaves="3"
              seed="2"
              baseFrequency="0.02 0.05"
            ></feTurbulence>
            <feDisplacementMap scale="7" in="SourceGraphic"></feDisplacementMap>
            <animate
              xlinkHref="#sea-filter"
              attributeName="baseFrequency"
              dur="60s"
              keyTimes="0;0.5;1"
              values="0.02 0.06;0.04 0.08;0.02 0.06"
              repeatCount="indefinite"
            />
          </filter>
        </svg>
      </div>
      {isMenuListOpen ? (
        <div className={classes.menu_list_container}>
          <ul>
            <li className={isMenuListOpen && classes.visible}>
              <Link
                href="/"
                onMouseEnter={(e: React.MouseEvent) =>
                  hover(e, classes.background_image)
                }
                onMouseLeave={hoveroff}
                onClick={() => (pathname === "/" ? toggle : null)}
              >
                <span className={soulmaze.className}>HOME</span>
              </Link>
            </li>
            <li className={isMenuListOpen && classes.visible}>
              <Link
                href="/work"
                onMouseEnter={(e: React.MouseEvent) =>
                  hover(e, classes.background_image_2)
                }
                onMouseLeave={hoveroff}
                onClick={() => (pathname === "/work" ? toggle : null)}
              >
                <span className={soulmaze.className}>WORK</span>
              </Link>
            </li>
            <li className={isMenuListOpen && classes.visible}>
              <Link href="/about">
                <span
                  onMouseEnter={(e) =>
                    hover(
                      e,
                      classes.background_image_3,
                      classes.background_image_3_mask
                    )
                  }
                  onMouseLeave={hoveroff}
                  onClick={() => (pathname === "/about" ? toggle : null)}
                  className={soulmaze.className}
                >
                  ABOUT
                </span>
              </Link>
            </li>
            <li className={isMenuListOpen && classes.visible}>
              <Link
                href="mailto:danielvaldezmontalvan@gmail.com"
                onMouseEnter={(e) => hover(e, classes.background_image_4)}
                onMouseLeave={hoveroff}
                target="_blank"
              >
                <span className={soulmaze.className}>EMAIL</span>
              </Link>
            </li>
            <li className={isMenuListOpen && classes.visible}>
              <Link
                href="https://github.com/DanValMont"
                onMouseEnter={(e) => hover(e, classes.background_image_5)}
                onMouseLeave={hoveroff}
                target="_blank"
              >
                <span className={soulmaze.className}>GITHUB</span>
              </Link>
            </li>
          </ul>
        </div>
      ) : (
        <div className={classes.menu_list_container}>
          <ul>
            <li className={`${classes.visible} ${classes.closing}`}>
              <Link href="/">
                <span className={soulmaze.className}>Home</span>
              </Link>
            </li>
            <li className={`${classes.visible} ${classes.closing}`}>
              <Link href="/work">
                <span className={soulmaze.className}>Work</span>
              </Link>
            </li>
            <li className={`${classes.visible} ${classes.closing}`}>
              <Link href="/about">
                <span className={soulmaze.className}>About</span>
              </Link>
            </li>
            <li className={`${classes.visible} ${classes.closing}`}>
              <Link
                href="mailto:danielvaldezmontalvan@gmail.com"
                target="_blank"
              >
                <span className={soulmaze.className}>Email</span>
              </Link>
            </li>
            <li className={`${classes.visible} ${classes.closing}`}>
              <Link href="https://github.com/DanValMont" target="_blank">
                <span className={soulmaze.className}>Github</span>
              </Link>
            </li>
          </ul>
        </div>
      )}
    </div>
  );
}

export default Menu;
