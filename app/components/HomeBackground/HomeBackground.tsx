"use client";
import React from "react";
import classes from "./HomeBackground.module.css";
import { useInvertContext } from "../../../context/Invert";

function Homebackground() {
  const { filter } = useInvertContext();
  return (
    <div className={classes.background_image} style={{ filter: filter }}>
      <div className={classes.background_image_with_transparency}></div>
    </div>
  );
}

export default Homebackground;
