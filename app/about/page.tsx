import React from "react";
import Aboutbackground from "./components/AboutBackground/AboutBackground";
// import Layout from "../components/Layout/Layout.js";
import Navbar from "../components/Navbar/Navbar";
import Aboutcontent from "./components/AboutContent/AboutContent";
import Footer from "./components/Footer/Footer";
import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'About - Daniel Valdez',
  description: `I'm Daniel Valdez, a web developer, and also an economist, who specializes in designing and developing
  front-end experiences to organizations, companies, start-ups and
  individuals.
  I use my passion and skills to create beautiful and scalable web
  products tailor-made for them.
  Are you minding a project and thinking we can make something great
  together? Let's talk over email.`,
  keywords: 'Daniel, Valdez, Montalvan, Daniel Valdez Montalvan, Portfolio, Web Developer, Full Stack Web Developer, Full Stack Developer, Website, Developer, About, Peru, Brasil, Brazil, US, United States, Freelance, Next.js, React, Prisma, MongoDB, Node.js, Express.js, PostgreSQL, Typescript, restaurant delivery web app, e-commerce web app, restaurant booking web app, video chat web app, Full Stack Application, Full Stack Projects'
}

function About() {
  return (
    <>
      <main>
        <section>
          <Navbar invertHeader={true} />
          <Aboutcontent />
          <Aboutbackground />
        </section>
      </main>
      <footer>
        <Footer />
      </footer>
      <svg xmlns="http://www.w3.org/2000/svg">
            <filter id="turbulence" x="0" y="0" width="100dvw" height="100dvh">
              <feTurbulence id="sea-filter" baseFrequency="0.02 0.05" numOctaves="3" seed="2" />
              <feDisplacementMap in="SourceGraphic" scale="7" />
              <animate
                attributeName="baseFrequency"
                dur="60s"
                keyTimes="0; 0.5; 1"
                repeatCount="indefinite"
                values="0.02 0.06; 0.04 0.08; 0.02 0.06"
                xlinkHref="#sea-filter"
              />
            </filter>
</svg>
</>
  );
}

export default About;
