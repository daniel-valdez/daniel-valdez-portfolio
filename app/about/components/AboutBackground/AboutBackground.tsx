"use client";
import React from "react";
import classes from "./AboutBackground.module.css";
import { useInvertContext } from "../../../../context/Invert";

function Aboutbackground() {
  const { filter } = useInvertContext();
  return (
    <div
      id="aboutmainbackground"
      className={classes.background_image}
      style={{filter: filter}}
    >
      <div className={classes.background_image_with_transparency}></div>
    </div>
  );
}

export default Aboutbackground;