"use client";
import React from "react";
import classes from "./AboutContent.module.css";
import Image from "next/image";
import { useInvertContext } from "../../../../context/Invert";
import Link from "next/link";
import { soulmazeItalic, madeFutureXthin } from "@/app/fonts/fonts";

function Aboutcontent() {
  const { filter } = useInvertContext();
  return (
    <div className={classes.container} style={{ filter: filter }}>
      <div>
        <h1 className={soulmazeItalic.className}>
          DANIEL
          <br />
          VALDEZ
        </h1>
      </div>
      <div className={`${classes.description} ${madeFutureXthin.className}`}>
        <p>
          I&apos;m a web developer, and also an economist, who specializes in
          designing and developing front-end experiences to organizations,
          companies, start-ups and individuals. I use my passion and skills to
          create beautiful and scalable web products tailor-made for them.
        </p>
        <p>
          Are you minding a project and thinking we can make something great
          together? Let&apos;s talk over email.
        </p>

        <span>
          <Link href="mailto:danielvaldezmontalvan@gmail.com" target="_blank">
            email
          </Link>
        </span>
        <span>
          <Link href="https://github.com/DanValMont" target="_blank">
            github
          </Link>
        </span>
        <span>
          <Link
            href="https://www.linkedin.com/in/daniel-borja-valdez-montalván-7121471b"
            target="_blank"
          >
            linkedin
          </Link>
        </span>
      </div>
      <div className={`${classes.technologies} ${madeFutureXthin.className}`}>
        <ul>
          <li>Technologies I use</li>
          <li>
            <div>Front-End</div>
            <div className={classes.technologies_imgs}>
              <Image
                src={"/icons/reactjs_icon.svg"}
                alt="react_js_icon"
                width={16}
                height={16}
              />
              <Image
                src={"/icons/materialui_icon.svg"}
                alt="material_ui_icon"
                width={16}
                height={16}
              />
              <Image
                src={"/icons/tailwindcss-about.svg"}
                alt="tailwind_css_icon"
                width={16}
                height={16}
              />
            </div>
          </li>
          <li>
            <div>Back-End</div>
            <div className={classes.technologies_imgs}>
              <Image
                src={"/icons/nodejs_icon.svg"}
                alt="node_js_icon"
                width={16}
                height={16}
              />
              <Image
                src={"/icons/mongodb_logo_icon.svg"}
                alt="mongo_db_logo_icon"
                width={16}
                height={16}
              />
              <Image
                src={"/icons/postgresql-about.svg"}
                alt="postgre_sql_icon"
                width={16}
                height={16}
              />
            </div>
          </li>
          <li>
            <div>Full-Stack</div>
            <div className={classes.technologies_imgs}>
              <Image
                src={"/icons/next-js.svg"}
                alt="next_js_icon"
                width={16}
                height={16}
              />
              <Image
                src={"/icons/typescript-about.svg"}
                alt="typescript_icon"
                width={16}
                height={16}
              />
            </div>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Aboutcontent;
