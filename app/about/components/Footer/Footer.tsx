"use client";
import { madeFutureXthin } from "@/app/fonts/fonts";
import React from "react";
import { useInvertContext } from "../../../../context/Invert";
import classes from "./Footer.module.css";

function Footer() {
  const { filter } = useInvertContext();

  return (
    <div className={classes.footer_container} style={{ filter: filter }}>
      <p className={madeFutureXthin.className}>&copy; 2023</p>
      <p className={madeFutureXthin.className}>
        designed and developed by Daniel Valdez
      </p>
    </div>
  );
}

export default Footer;
