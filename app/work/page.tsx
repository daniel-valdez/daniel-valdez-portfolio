import React from "react";
import Navbar from "../components/Navbar/Navbar";
import type { Metadata } from "next";
import ProjectsContainer from "./components/ProjectsContainer/ProjectsContainer";

export const metadata: Metadata = {
  title: "Work - Daniel Valdez",
  description: `Portfolio of selected website applications projects developed by Daniel Valdez`,
  keywords:
    "Daniel, Valdez, Montalvan, Daniel Valdez Montalvan, Portfolio, Web Developer, Full Stack Web Developer, Full Stack Developer, Website, Developer, Peru, Brasil, Brazil, US, United States, Freelance, Next.js, React, Prisma, MongoDB, Node.js, Express.js, PostgreSQL, Typescript, restaurant delivery web app, e-commerce web app, restaurant booking web app, video chat web app, Full Stack Application, Full Stack Projects",
};

function Projects() {
  return (
    <main>
      <section>
        <Navbar invertHeader={true} invertEffect={true} />
        <ProjectsContainer />
      </section>
    </main>
  );
}

export default Projects;
