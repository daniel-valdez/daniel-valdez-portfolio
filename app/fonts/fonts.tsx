import localFont from "next/font/local";

const soulmaze = localFont({
  src: "./Soulmaze/Soulmaze.woff2",
  style: "normal",
  display: "swap",
});
const soulmazeItalic = localFont({
  src: "./Soulmaze/Soulmaze-Italic.woff2",
  style: "italic",
  display: "swap",
});
const soulmazeBrush = localFont({
  src: "./Soulmaze/Soulmaze-Brush.woff2",
  style: "normal",
  display: "swap",
});
const questrial = localFont({
  src: "./Questrial/Questrial-Regular.woff2",
  style: "normal",
  display: "swap",
});
const madeFutureXthin = localFont({
  src: "./MADE Future X/MADE Future X Thin.woff2",
  style: "normal",
  display: "swap",
});

export { soulmaze, soulmazeItalic, soulmazeBrush, questrial, madeFutureXthin };
