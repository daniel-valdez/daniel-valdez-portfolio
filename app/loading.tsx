import { soulmazeItalic } from "./fonts/fonts";
import classes from "./loading.module.css";

function Loading() {
  return (
    <div className={classes.loader_container}>
      <h2 className={`${classes.loader} ${soulmazeItalic.className}`}>
        Loading
      </h2>
    </div>
  );
}

export default Loading;
